# 8. group_by

Date: 2023-03-31

## Status

Accepted

## Context

We need to be able to query in DDS the L3 device where one IP CIDR is connected.
All the vendors/devices will be normalized.


## Decision

We use dsm model with "group_by" key

- Example

  here we are grouping the staticpath under the Tenants
  ![Tenants](screenshot_for_group_by.png)

  - dsm model for the above screenshot

  ```python
  "staticpath": {
    "title": "Staticpath",
    "type": "list",
    "description": "",
    "description_source": "https://pubhub.devnetcloud.com.mcas.ms/media/model-doc-521/docs/doc/jsonmeta/fv/RsPathAtt.json?McasTsid=10549",
    "group_by": "Tenants",
    "source": {
        "type": "dn_lookup",
        "dn": "domain.staticpath",
        "source": "input"
  }
  ```

## Consequences

- "group_by" is used to group the domains under a specific domain in details view.
- upate model generator,dsm model and filter from the output
